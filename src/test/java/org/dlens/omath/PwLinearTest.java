package org.dlens.omath;

import org.dlens.omath.percentile.GradingCompatiblePercentileException;
import org.dlens.omath.pwlinear.PwLinear;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PwLinearTest {
    private static final double ERR = 1e-14;
    private static final double DECAY = 0.01;

    @Test
    public void test() {
        PwLinear pwlinear = new PwLinear(
                new double[]{0.75, 1.0, 1.1},
                new double[]{0.95, 0.5, 0.1},
                1, 0, 10000);
        assertEquals(0.95, pwlinear.eval(0.75), ERR);
        assertEquals(0.5, pwlinear.eval(1.0), ERR);
        assertEquals(0.1, pwlinear.eval(1.1), ERR);

    }

    @Test
    public void test2() {
        PwLinear pwLinear = new PwLinear(new double[]{10, 20, 30, 40, 50}, new double[]{DECAY, 1d / 4, 2d / 4, 3d / 4, 1 - DECAY}, 0, 1, 10000);
        double[] x = new double[]{22, 24, 26, 28, 30};
        double[] y = new double[]{0.3, 0.35, 0.4, 0.45, 0.5};

        for (int i = 0; i < x.length; i++) {
            assertEquals(pwLinear.eval(x[i]), y[i], ERR);
            assertEquals(pwLinear.evalInverse(y[i]), x[i], ERR);
        }
    }

    @Test
    public void testInvalid() {
        assertThrows(GradingCompatiblePercentileException.class, () -> new PwLinear(new double[]{1}, new double[]{0, 1}, 0, 0, 0));
    }

//		assertEquals(0.01, pwlinear.eval(1.4), err);
//		int count = 1000;
//		double min=0.5, max=1.5;
//		List<Double> xs = new ArrayList<Double>(count);
//		List<Double> ys = new ArrayList<Double>(count);
//		double x, y;
//		for(int i=0; i < count; i++) {
//			x = min+ (i+0.0)/count * (max-min);
//			y = pwlinear.eval(x);
//			xs.add(x);
//			ys.add(y);
//		}
//		Plot plt = Plot.create();
//		plt.plot().add(xs, ys);
//		plt.show();
}
