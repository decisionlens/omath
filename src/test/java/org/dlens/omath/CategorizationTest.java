package org.dlens.omath;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CategorizationTest {
    private static final double ERR = 1e-10;
    private final StandardLikertScale P = StandardLikertScale.VERY_POOR;
    private final StandardLikertScale p = StandardLikertScale.POOR;
    private final StandardLikertScale f = StandardLikertScale.FAIR;
    private final StandardLikertScale g = StandardLikertScale.GOOD;
    private final StandardLikertScale G = StandardLikertScale.VERY_GOOD;

    @Test
    public void testCategorizationSmall() {
        double[] vals = {1.1, 5, 7, 1.1, 5};
        Categorization cat = new Categorization();
        StandardLikertValue[] scales = cat.standardCategoriesZscore(vals);
        assertArrayEquals(new StandardLikertScale[]{p, f, g, p, f}, StandardLikertValue.scales(scales));
        vals = new double[]{3.1, 1.1, 5., 7., 1.1, 5.};
        scales = cat.standardCategoriesZscore(vals);
        assertArrayEquals(new StandardLikertScale[]{f, p, f, g, p, f}, StandardLikertValue.scales(scales));
    }

    @Test
    public void testCategorizationZscore() {
        double[] vals = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7};
        Categorization cat = new Categorization();
        StandardLikertValue[] scales = cat.standardCategoriesZscore(vals);
        StandardLikertScale[] expected = new StandardLikertScale[]{P, P, P, P, p, p, p, f, f, f, g, g, g, G, G, G, G};
        assertArrayEquals(expected, StandardLikertValue.scales(scales));
        System.out.println(Arrays.asList(scales));
    }

    @Test
    public void categorizationForZscores() {
        double[] vals = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7};
        double[] zscores = OmathUtils.zscore(vals);
        Categorization cat = new Categorization();
        StandardLikertValue[] scales = cat.standardCategoriesForZscores(zscores, vals, false);
        StandardLikertScale[] expected = new StandardLikertScale[]{P, P, P, P, p, p, p, f, f, f, g, g, g, G, G, G, G};
        assertArrayEquals(expected, StandardLikertValue.scales(scales));
        double[] expectedOriginalValues = Arrays.stream(scales).map(s -> s.originalValue).mapToDouble(Double::doubleValue).toArray();
        assertArrayEquals(expectedOriginalValues, vals, 0.0);
    }

    @Test
    public void preserveOriginalValueWhenClustering() {
        double[] vals = {1.0, 1.001, 1.002, 1.01, 1.99, 1.995, 2, 2.001};
        Categorization cat = new Categorization();
        StandardLikertValue[] scales = cat.standardCategoriesZscore(vals);
        StandardLikertScale[] expected = new StandardLikertScale[]{p, p, p, p, g, g, g, g};
        assertArrayEquals(expected, StandardLikertValue.scales(scales));
        double[] expectedOriginalValues = Arrays.stream(scales).map(s -> s.originalValue).mapToDouble(Double::doubleValue).toArray();
        assertArrayEquals(expectedOriginalValues, vals, 0.0);
    }

    @Test
    public void testCluster1() {
        double[] vals = {1.0, 1.001, 1.002, 1.01, 1.99, 1.995, 2, 2.001};
        Categorization cat = new Categorization();
        List<List<Integer>> clusters = cat.cluster(vals);
        assertEquals(2, clusters.size());
        assertEquals(4, clusters.get(0).size());
        assertEquals(0, clusters.get(0).get(0).intValue());
        assertEquals(1, clusters.get(0).get(1).intValue());
        assertEquals(2, clusters.get(0).get(2).intValue());
        assertEquals(3, clusters.get(0).get(3).intValue());
        assertEquals(4, clusters.get(1).size());
        assertEquals(4, clusters.get(1).get(0).intValue());
        assertEquals(5, clusters.get(1).get(1).intValue());
        assertEquals(6, clusters.get(1).get(2).intValue());
        assertEquals(7, clusters.get(1).get(3).intValue());
    }

    @Test
    public void testCluster1Rand() {
        double[] vals = {2.0, 2.001, 1.01, 1.002, 1.99, 1.995, 1.0, 1.001};
        Categorization cat = new Categorization();
        List<List<Integer>> clusters = cat.cluster(vals);
        assertEquals(2, clusters.size());
        assertEquals(4, clusters.get(0).size());
        assertEquals(6, clusters.get(0).get(0).intValue());
        assertEquals(7, clusters.get(0).get(1).intValue());
        assertEquals(3, clusters.get(0).get(2).intValue());
        assertEquals(2, clusters.get(0).get(3).intValue());
        assertEquals(4, clusters.get(1).size());
        assertEquals(4, clusters.get(1).get(0).intValue());
        assertEquals(5, clusters.get(1).get(1).intValue());
        assertEquals(0, clusters.get(1).get(2).intValue());
        assertEquals(1, clusters.get(1).get(3).intValue());
    }

    @Test
    public void testCategorization1Rand() {
        double[] vals = {2.0, 2.001, 1.01, 1.002, 1.99, 1.995, 1.0, 1.001};
        Categorization cat = new Categorization();
        StandardLikertValue[] cats = cat.standardCategoriesZscore(vals);
        assertArrayEquals(new StandardLikertScale[]{g, g, p, p, g, g, p, p}, StandardLikertValue.scales(cats));
    }

    @Test
    public void testCategorization1SkewedRand() {
        double[] vals = {2.0, 2.001, 1.01, 1.002, 1.99, 1.995, 1.0, 1.001, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
        Categorization cat = new Categorization();
        StandardLikertValue[] cats = cat.standardCategoriesZscore(vals);
        assertArrayEquals(new StandardLikertScale[]{g, g, p, p, g, g, p, p, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g}, StandardLikertValue.scales(cats));
        cat.params.useClustering = false;
        cats = cat.standardCategoriesZscore(vals);
        assertArrayEquals(new StandardLikertScale[]{g, G, f, p, f, g, P, p, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g, g}, StandardLikertValue.scales(cats));
    }

    @Test
    public void testCluster1RandWithIdeal() {
        double small = 100000000.;
        double[] vals = {1 + 2.0 / small, 1 + 2.001 / small, 1 + 1.01 / small, 1 + 1.002 / small, 1 + 1.99 / small, 1 + 1.995 / small, 1 + 1.0 / small, 1 + 1.001 / small};
        Categorization cat = new Categorization();
        cat.params.clusteringNormalize = NormalizationType.IDEAL;
        cat.params.clusteringEpsilon = 0.01;
        List<List<Integer>> clusters = cat.cluster(vals);
        assertEquals(2, clusters.size());
        assertEquals(4, clusters.get(0).size());
        assertEquals(6, clusters.get(0).get(0).intValue());
        assertEquals(7, clusters.get(0).get(1).intValue());
        assertEquals(3, clusters.get(0).get(2).intValue());
        assertEquals(2, clusters.get(0).get(3).intValue());
        assertEquals(4, clusters.get(1).size());
        assertEquals(4, clusters.get(1).get(0).intValue());
        assertEquals(5, clusters.get(1).get(1).intValue());
        assertEquals(0, clusters.get(1).get(2).intValue());
        assertEquals(1, clusters.get(1).get(3).intValue());
        //Without zscore should give 8 clusters
        cat.params.clusteringNormalize = NormalizationType.NONE;
        clusters = cat.cluster(vals);
        assertEquals(1, clusters.size());
        int[] sortedIndex = {6, 7, 3, 2, 4, 5, 0, 1};
        for (int i = 0; i < 8; i++)
            assertEquals(sortedIndex[i], clusters.get(0).get(i).intValue());
//        assertEquals(8, clusters.size());
//        int[] sortedIndex = {6, 7, 3, 2, 4, 5, 0, 1};
//        for(int i=0; i<8; i++) {
//            assertEquals(1, clusters.get(i).size());
//            assertEquals(sortedIndex[i], clusters.get(i).get(0).intValue());
//        }
    }

    @Test
    public void testIdealsSmall() {
        double[] vals = {1.1, 5, 7, 1.1, 5};
        Categorization cat = new Categorization();
        double[] scales = StandardLikertValue.likertValues(cat.standardCategoriesZscore(vals, false));
        assertArrayEquals(new double[]{.25, .5, .75, .25, .5}, scales, ERR);
        vals = new double[]{3.1, 1.1, 5., 7., 1.1, 5.};
        scales = StandardLikertValue.likertValues(cat.standardCategoriesZscore(vals, false));
        assertArrayEquals(new double[]{0.5, 0.25, 0.5, 0.75, 0.25, 0.50}, scales, ERR);
    }

    @Test
    public void testIdealsZscore() {
        double[] vals = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7};
        Categorization cat = new Categorization();
        double[] scales = StandardLikertValue.likertValues(cat.standardCategoriesZscore(vals, false));
        System.out.println(Arrays.toString(scales));
        double P = 0, p = 0.25, f = 0.5, g = 0.75, G = 1;
        double[] expected = new double[]{
                0.055960908058333036, 0.07772512115924793, 0.1079538318592417, 0.1499390369455449,
                0.20854181674528985, 0.2779394444233696, 0.3473370721014494,
                0.4194290579083277, 0.5000000000000001, 0.5805709420916725,
                0.6526629278985507, 0.7220605555766305, 0.7914581832547103,
                0.8500609630544552, 0.8920461681407584, 0.9222748788407522, 0.944039091941667};
        assertArrayEquals(expected, scales, 1e-5);
        System.out.println(List.of(scales));
    }


}