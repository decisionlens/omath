package org.dlens.omath;

import lombok.RequiredArgsConstructor;
import org.dlens.omath.percentile.GradingCompatiblePercentile.Percentile;
import org.dlens.omath.percentile.GradingCompatiblePercentileException;
import org.junit.jupiter.api.Test;

import static java.lang.Double.NaN;
import static org.dlens.omath.percentile.GradingCompatiblePercentile.computeGradingCompatiblePercentiles;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RequiredArgsConstructor
public class GradingCompatiblePercentileTest {
    private static final double ERR = 1e-14;
    private static final double DECAY = 0.01;

    @Test
    public void xiPercentiles() {
        double[] x0_100 = {70, 100, 30, 70, 10, 60, 20, NaN, 80, 90, 80, 50, 20, 10, NaN, 60, 100, 40, 0, 50};
        Percentile percentile = computeGradingCompatiblePercentiles(x0_100);
        assertArrayEquals(new double[]{0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100}, percentile.getX(), ERR);
        assertArrayEquals(new double[]{0 + DECAY, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1 - DECAY}, percentile.getY(), ERR);
    }

    @Test
    public void noValidElements() {
        assertThrows(GradingCompatiblePercentileException.class, () -> computeGradingCompatiblePercentiles(new double[]{NaN, NaN, NaN}));
    }

    @Test
    public void oneValidElements() {
        Percentile percentile = computeGradingCompatiblePercentiles(new double[]{NaN, NaN, 1, NaN});
        assertArrayEquals(new double[]{1}, percentile.getX(), ERR);
        assertArrayEquals(new double[]{0.5}, percentile.getY(), ERR);
    }

}
