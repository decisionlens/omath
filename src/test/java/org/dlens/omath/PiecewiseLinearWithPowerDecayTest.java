package org.dlens.omath;

import org.dlens.omath.percentile.GradingCompatiblePercentileException;
import org.dlens.omath.pwlinear.PiecewiseLinearWithPowerDecay;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.offset;
import static org.dlens.omath.percentile.GradingCompatiblePercentile.Percentile;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PiecewiseLinearWithPowerDecayTest {
    private static final double ERR = 1e-10;
    private static final double DECAY = 0.01;

    @Test
    public void testElementsEvalSimple() {
        double[] xs = {0.75, 1.0, 1.1};
        double[] ys = {0.95, 0.5, 0.1};
        PiecewiseLinearWithPowerDecay pwlinear = new PiecewiseLinearWithPowerDecay(xs, ys, 1, 0, 1);

        assertThat(pwlinear.eval(0.75)).isEqualTo(0.95, offset(ERR));
        assertThat(pwlinear.eval(1.0)).isEqualTo(0.5, offset(ERR));
        assertThat(pwlinear.eval(1.1)).isEqualTo(0.09999999999999998, offset(ERR));
        assertThat(pwlinear.getX()).isEqualTo(xs);
    }

    @Test
    public void testElementsEvalComplex() {
        double[] x0 = {10, 20, 30, 40, 50};
        double[] y0 = {DECAY, 1d / 4, 2d / 4, 3d / 4, 1 - DECAY};
        Percentile percentile = new Percentile(x0, y0);
        PiecewiseLinearWithPowerDecay pwLinear = new PiecewiseLinearWithPowerDecay(percentile);
        double[] x = new double[]{5, 22, 24, 26, 28, 30, 55};
        double[] y = new double[]{7.692307692307681E-4, 0.3, 0.35, 0.4, 0.45, 0.5, 0.9992307692307693};

        for (int i = 0; i < x.length; i++) {
            assertThat(pwLinear.eval(x[i])).isEqualTo(y[i], offset(ERR));
            assertThat(pwLinear.evalInverse(y[i])).isEqualTo(x[i], offset(ERR));
        }

        double[] evalRes = pwLinear.eval(x);
        double[] inverseEvalRes = pwLinear.evalInverse(y);
        for (int i = 0; i < evalRes.length; i++) {
            assertThat(evalRes[i]).isEqualTo(y[i], offset(ERR));
            assertThat(inverseEvalRes[i]).isEqualTo(x[i], offset(ERR));
        }
    }

    @Test
    public void testArrayEval() {
        double[] x0 = {10, 20, 30, 40, 50};
        double[] y0 = {DECAY, 1d / 4, 2d / 4, 3d / 4, 1 - DECAY};
        Percentile percentile = new Percentile(x0, y0);
        PiecewiseLinearWithPowerDecay pwLinear = new PiecewiseLinearWithPowerDecay(percentile);
        double[] x = new double[]{5, 22, 24, 26, 28, 30, 55};
        double[] y = new double[]{7.692307692307681E-4, 0.3, 0.35, 0.4, 0.45, 0.5, 0.9992307692307693};

        double[] evalRes = pwLinear.eval(x);
        double[] inverseEvalRes = pwLinear.evalInverse(y);
        for (int i = 0; i < evalRes.length; i++) {
            assertThat(evalRes[i]).isEqualTo(y[i], offset(ERR));
            assertThat(inverseEvalRes[i]).isEqualTo(x[i], offset(ERR));
        }
    }

    @Test
    public void testXYNotSameLength() {
        assertThrows(GradingCompatiblePercentileException.class, () -> new PiecewiseLinearWithPowerDecay(new double[]{1}, new double[]{0, 1}, 0, 0, 1));
    }

    @Test
    public void testNanEvalAndOneElementPercentile() {
        PiecewiseLinearWithPowerDecay pwLinear =
                new PiecewiseLinearWithPowerDecay(new double[]{1}, new double[]{2}, 0, 0, 1);
        assertThat(Double.isNaN(pwLinear.eval(Double.NaN))).isTrue();
        assertThat(Double.isNaN(pwLinear.evalInverse(Double.NaN))).isTrue();
        assertThat(pwLinear.eval(1)).isEqualTo(2);
        assertThat(pwLinear.evalInverse(2)).isEqualTo(1);
    }

}
