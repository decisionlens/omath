package org.dlens.omath;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class OmathUtilsTest {
    private static final double err = 1e-10;


    @Test
    public void testMean() {
        assertEquals(0.0, OmathUtils.mean(null), err);
        assertEquals(0.0, OmathUtils.mean(new double[]{}), err);
        assertEquals(7. / 3, OmathUtils.mean(new double[]{1, 2, 4}), err);
    }

    @Test
    public void testMin() {
        assertEquals(Double.POSITIVE_INFINITY, OmathUtils.min(null), err);
        assertEquals(Double.POSITIVE_INFINITY, OmathUtils.min(new double[]{}), err);
        assertEquals(1, OmathUtils.min(new double[]{1, 2, 4}), err);
    }

    @Test
    public void testMax() {
        assertEquals(Double.NEGATIVE_INFINITY, OmathUtils.max(null), err);
        assertEquals(Double.NEGATIVE_INFINITY, OmathUtils.max(new double[]{}), err);
        assertEquals(4, OmathUtils.max(new double[]{1, 2, 4}), err);
    }

    @Test
    public void testConvertKMG() {
        assertEquals("1", OmathUtils.convertToKMGNotation(1, 1));
        assertEquals("12", OmathUtils.convertToKMGNotation(12, 2));
        assertEquals("100", OmathUtils.convertToKMGNotation(123, 1));
        assertEquals("120", OmathUtils.convertToKMGNotation(123, 2));
        assertEquals("123", OmathUtils.convertToKMGNotation(123, 3));
        assertEquals("200", OmathUtils.convertToKMGNotation(153, 1));
        assertEquals("2K", OmathUtils.convertToKMGNotation(1530, 1));
        assertEquals("2.1M", OmathUtils.convertToKMGNotation(2134098, 2));
    }

    @Test
    public void testEdgeOfPoint() {
        double[] xs = {0, 1, 2, 0, 1, 2, 0, 1, 2};
        double[] ys = {0, 0, 0, 1, 1, 1, 2, 2, 2};
        List<double[]> edge = OmathUtils.edgeOfPoints(xs, ys, 1, 1);
        assertEquals(6, edge.size());
        for (double[] e : edge)
            System.out.println(Arrays.toString(e));
    }

    @Test
    public void testUniqueVals() {
        List<Double> vals = Categorization.uniqueValues(new double[]{1.1, 5, 7, 1.1, 5});
        assertEquals(vals, Arrays.asList(1.1, 5.0, 7.0));
    }

    @Test
    public void zscores_withMeanAndStdDev() {
        double[] vals = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7};
        double mean = OmathUtils.mean(vals);
        double stddev = OmathUtils.stddev(vals);
        double[] zscoresWithMeanAndStdev = OmathUtils.zscore(vals, mean, stddev);
        double[] zscores = OmathUtils.zscore(vals);
        Assertions.assertThat(zscoresWithMeanAndStdev).containsExactly(zscores);
    }

    @Test
    public void transposeTwoDimensionalArrayTest() {
        double[][] arr = new double[][]{
                new double[]{1, 1, 1, 1, 1, 1}, new double[]{2, 2, 2, 2, 2, 2}, new double[]{3, 3, 3, 3, 3, 3},
                new double[]{4, 4, 4, 4, 4, 4}, new double[]{5, 5, 5, 5, 5, 5}, new double[]{6, 6, 6, 6, 6, 6}};
        double[][] result = OmathUtils.transposeTwoDimensionalArray(arr);

        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[0], 0);
        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[1], 0);
        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[2], 0);
        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[3], 0);
        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[4], 0);
        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[5], 0);
    }

    @Test
    public void transposeTwoDimensionalArrayTest0() {
        double[][] arr = new double[][]{
                new double[]{1, 1, 1, 1, 1, 1}, new double[]{2, 2, 2, 2, 2, 2}, new double[]{3, 3, 3, 3, 3, 3},
                new double[]{4, 4, 4, 4, 4, 4}, new double[]{5, 5, 5, 5, 5, 5}, new double[]{6, 6, 6, 6, 6, 6}};
        double[][] result = OmathUtils.transposeTwoDimensionalArray(arr, 6, 6);

        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[0], 0);
        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[1], 0);
        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[2], 0);
        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[3], 0);
        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[4], 0);
        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[5], 0);
    }

    @Test
    public void transposeTwoDimensionalArrayTest1() {
        double[][] arr = new double[][]{new double[]{1, 1, 1, 1, 1, 1}, new double[]{2, 2, 2, 2, 2, 2}, new double[]{3, 3, 3, 3, 3, 3}};
        double[][] result = OmathUtils.transposeTwoDimensionalArray(arr, 3, 6);

        assertArrayEquals(new double[]{1, 2, 3}, result[0], 0);
        assertArrayEquals(new double[]{1, 2, 3}, result[1], 0);
        assertArrayEquals(new double[]{1, 2, 3}, result[2], 0);
        assertArrayEquals(new double[]{1, 2, 3}, result[3], 0);
        assertArrayEquals(new double[]{1, 2, 3}, result[4], 0);
        assertArrayEquals(new double[]{1, 2, 3}, result[5], 0);
    }

    @Test
    public void transposeTwoDimensionalArrayTest2() {
        double[][] arr = new double[][]{
                new double[]{1, 1}, new double[]{2, 2}, new double[]{3, 3},
                new double[]{4, 4}, new double[]{5, 5}, new double[]{6, 6}};
        double[][] result = OmathUtils.transposeTwoDimensionalArray(arr, 6, 2);

        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[0], 0);
        assertArrayEquals(new double[]{1, 2, 3, 4, 5, 6}, result[1], 0);
    }
}
