package org.dlens.omath;

import org.dlens.omath.pwlinear.PwLinear;

import java.util.*;

public class Categorization {
    private static final int SMALL_DATA_SIZE_LIMIT = 12;
    public static final double ZSCORE_CUTOFFS[] = {-0.84162123, -0.2533471 ,  0.2533471 ,  0.84162123};
    public static final double ZSCORE_CUTOFF_VALUES[] = {0.2, 0.4, 0.6, 0.8};
    public static final PwLinear ZSCORE_INTERPRETTER = new PwLinear(ZSCORE_CUTOFFS, ZSCORE_CUTOFF_VALUES, 0, 1, 5);

    public CategorizationParams params = CategorizationParams.factoryDefault();
    public static final StandardLikertScale L = StandardLikertScale.VERY_POOR, l = StandardLikertScale.POOR, m=StandardLikertScale.FAIR, h=StandardLikertScale.GOOD, H=StandardLikertScale.VERY_GOOD;
    public static final StandardLikertScale[][] smallStandardScales = {
            {m},
            {l,h},
            {l,m,h},
            {l,m,m,h},
            {L,l,m,h,H},
            {L,l,m,m,h,H},
            {L,l,l,m,h,h,H},
            {L,l,l,m,m,h,h,H},
            {L,L,l,l,m,h,h,H,H},
            {L,L,l,l,m,m,h,h,H,H},
            {L,L,l,l,m,m,m,h,h,H,H},
            {L,L,l,l,l,m,m,h,h,h,H,H},
            {L,L,l,l,l,m,m,m,h,h,h,H,H},
            {L,L,L,l,l,l,m,m,h,h,h,H,H,H},
            {L,L,L,l,l,l,m,m,m,h,h,h,H,H,H}
    };

    public Categorization() {

    }

    public Categorization(CategorizationParams categorizationParams) {
        params = categorizationParams;
    }

    public StandardLikertValue[] standardCategoriesZscore(double vals[]) {
        return standardCategoriesZscore(vals, false);
    }

    /** Uses the zscores of the values and the 20, 40, 60, 80% cutoffs of the normal
     * distribution to calculate the Very Low, to Very High categorization of the values.
     * If the number of values is small, it does a specialized calculation.
     * @param vals
     * @return
     */
    public StandardLikertValue[] standardCategoriesZscore(double vals[], boolean reversed) {
        if (vals==null)
            return null;
        else if (vals.length == 0)
            return new StandardLikertValue[0];
        //If we should cluster first, do so
        double[] originalValues = vals;
        if (params.useClustering) {
            originalValues = Arrays.copyOf(vals, vals.length);
            vals = clusteredValues(vals);
        }
        //Check if we have a small number of values
        List<Double> uniqueVals = uniqueValues(vals, reversed);
        if (uniqueVals.size() < SMALL_DATA_SIZE_LIMIT) {
            //Use the small number version of this algorithm
            return standardCategoriesSmallSize(vals, originalValues, uniqueVals, reversed);
        }
        /*These are the inverse cumulative normal distribution values for 20%, 40%, 60% and 80%*/
        /*They define our cutoffs between differing values*/
        double cutoffs[] = {-0.84162123, -0.2533471 ,  0.2533471 ,  0.84162123};
        double[] zscores = OmathUtils.zscore(vals);
        return standardCategoriesForZscores(zscores, originalValues, reversed);
    }

    public boolean isSmallDataSize(double[] vals) {
        List<Double> uniqueVals = uniqueValues(vals);
        return uniqueVals.size() < SMALL_DATA_SIZE_LIMIT;
    }

    public StandardLikertValue[] standardCategoriesForZscores(double[] zscores, double[] originalValues, boolean reversed) {
        StandardLikertValue[] rval = new StandardLikertValue[zscores.length];
        double likertValue, val;
        for (int i = 0; i < zscores.length; i++) {
            val = reversed ? -zscores[i] : zscores[i];
            likertValue = ZSCORE_INTERPRETTER.eval(val);
            rval[i] = new StandardLikertValue(originalValues[i], likertValue);
        }
        return rval;
    }

    public static List<Double> uniqueValues(double vals[]) {
        return uniqueValues(vals, false);
    }

    public static List<Double> uniqueValues(double vals[], boolean reversed) {
        HashSet<Double> uniques = new HashSet();
        for(double val : vals)
            uniques.add(val);
        ArrayList<Double> vs = new ArrayList(uniques);
        if (reversed)
            Collections.sort(vs, Collections.reverseOrder());
        else
            Collections.sort(vs);
        return vs;
    }


    public StandardLikertValue[] standardCategoriesSmallSize(double[] vals, double[] originalValues, List<Double> uniqueVals, boolean reversed) {
        if (uniqueVals == null)
            uniqueVals = uniqueValues(vals, reversed);
        if (vals==null)
            return null;
        else if (vals.length == 0)
            return new StandardLikertValue[0];
        //Check if we have a small number of values
        int nuniques = uniqueVals.size();
        if (uniqueVals.size() >= smallStandardScales.length)
            throw new UnsupportedOperationException("Cannot use small size categorization algorithm with "+vals.length+" items");
        StandardLikertValue[] rval = new StandardLikertValue[vals.length];
        int index;
        StandardLikertScale[] scaleToUse = smallStandardScales[nuniques-1];
        for (int i=0; i < vals.length; i++) {
            index = uniqueVals.indexOf(vals[i]);
            rval[i] = new StandardLikertValue(originalValues[i], scaleToUse[index]);
        }
        return rval;
    }

    public double[] clusteredValues(double vals[]) {
        List<List<Integer>> clusters = cluster(vals);
        double rval[] = new double[vals.length];
        double val;
        double[] clusterCenters = new double[clusters.size()];
        List<Integer> cluster;
        for(int i=0; i < clusterCenters.length; i++) {
            clusterCenters[i] = 0;
            cluster = clusters.get(i);
            for(Integer index : cluster) {
                clusterCenters[i]+=vals[index];
            }
            clusterCenters[i] /= cluster.size();
        }
        //Now that we know the centers of each cluster, we use that center as the value
        for(int i=0; i < clusterCenters.length; i++) {
            cluster = clusters.get(i);
            val = clusterCenters[i];
            for(Integer index : cluster) {
                rval[index] = val;
            }
        }
        return rval;
    }

    public List<List<Integer>> cluster(double thevals[]) {
        if (params.clusteringEpsilon < 0)
            throw new IllegalArgumentException("Epsilon must be greater than zero");
        if (params.clusteringDelta < 0)
            throw new IllegalArgumentException("delta must be greater than zero");
        if (params.clusteringDelta <= params.clusteringEpsilon)
            throw new IllegalArgumentException("delta must be bigger than epsilon");
        thevals = params.clusteringNormalize.normalize(thevals);
        final double[] vals = thevals;
        List<Integer> sortedIndex = OmathUtils.range(vals.length);
        Collections.sort(sortedIndex, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                return Double.compare(vals[i1], vals[i2]);
            }
        });
        double max = OmathUtils.max(vals);
        double scale = (max == 0) ? 1.0 : max;
        List<List<Integer>> rval = new LinkedList<>();
        ArrayList<Integer> current = new ArrayList<>();
        rval.add(current);
        Double lastValue = null, val;
        for(Integer index : sortedIndex) {
            val = vals[index];
            if (lastValue == null)
                lastValue = val;
            if (val - lastValue < params.clusteringEpsilon*scale) {
                //We can add this to the cluster, however the cluster might be too big now
                current.add(index);
                if (vals[current.get(current.size()-1)] - vals[current.get(0)] > params.clusteringDelta*scale) {
                    //The last one in rval is the current, we must remove it
                    rval.remove(rval.size()-1);
                    //We have gotten too big, break up into individual clusters
                    for (Integer cval : current) {
                        rval.add(Arrays.asList(cval));
                    }
                    lastValue = null;
                    current = new ArrayList<>();
                } else {
                    lastValue = val;
                }
            } else {
                //We have a cluster, add it and get ready to restart
                current = new ArrayList<>();
                current.add(index);
                rval.add(current);
                lastValue = vals[index];
            }
        }
        return rval;
    }


//    /** Uses the zscores of the values and the 20, 40, 60, 80% cutoffs of the normal
//     * distribution to calculate the Very Low, to Very High categorization of the values.
//     * If the number of values is small, it does a specialized calculation.  However rather
//     * than returning categories, it linearly interpolates the original scores to numbers
//     * between 0 = Very Poor and 1=Very Good.
//     * @param vals
//     * @param reversed Is the ranking reversed.
//     * @return
//     */
//    public double[] standardIdealsWithZscore(double vals[], boolean reversed) {
//        if (vals==null)
//            return null;
//        else if (vals.length == 0)
//            return new double[0];
//        //If we should cluster first, do so
//        if (params.useClustering) {
//            vals = clusteredValues(vals);
//        }
//        //Check if we have a small number of values
//        List<Double> uniqueVals = uniqueValues(vals, reversed);
//        if (uniqueVals.size() < 12) {
//            //Use the small number version of this algorithm
//            return standardIdealsSmallSize(vals, uniqueVals, reversed);
//        }
//        double[] rval = new double[vals.length];
//        /*These are the inverse cumulative normal distribution values for 20%, 40%, 60% and 80%*/
//        /*They define our cutoffs between differing values*/
//        double[] zscores = OmathUtils.zscore(vals);
//        double val;
//        for(int i=0; i < vals.length; i++) {
//            if (reversed)
//                val = -zscores[i];
//            else
//                val = zscores[i];
//            rval[i] = ZSCORE_INTERPRETTER.eval(val);
//        }
//        return rval;
//    }
//

}
