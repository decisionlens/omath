package org.dlens.omath;

public enum StandardLikertScale {
    VERY_POOR,
    POOR,
    FAIR,
    GOOD,
    VERY_GOOD;

    public static StandardLikertScale fromLikertValue(double likertValue) {
        if (likertValue < 0.2) {
            return VERY_POOR;
        } else if (likertValue < 0.4) {
            return POOR;
        } else if (likertValue < 0.6) {
            return FAIR;
        } else if (likertValue < 0.8) {
            return GOOD;
        } else {
            return VERY_GOOD;
        }
    }

    public double doubleValue() {
        return switch (this) {
            case VERY_POOR -> 0.00;
            case POOR -> 0.25;
            case FAIR -> 0.50;
            case GOOD -> 0.75;
            case VERY_GOOD -> 1.00;
            default -> throw new UnsupportedOperationException("Unknwon type " + this);
        };
    }
}
