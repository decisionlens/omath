package org.dlens.omath.pwlinear;

import lombok.Setter;
import org.apache.commons.math3.util.FastMath;
import org.dlens.omath.percentile.GradingCompatiblePercentileException;

public class PwLinear extends AbstractPwLinear {
	private double lower;
	private double upper;
	private double logBase;

	@Setter
	private boolean useExponential = true;

	public PwLinear(double[] xs, double[] ys, double lower, double upper, double base) {
		if (xs.length != ys.length)
			throw new GradingCompatiblePercentileException("Invalid input: xs size should be the same as ys size");

		this.xs = xs;
		this.ys = ys;
		this.lower = lower;
		this.upper = upper;
		logBase = FastMath.log(base);
	}

	public double eval(double x) {
		int n = xs.length;
		if (x < xs[0]) {
			// In lower bound area -infty return lower xs[0] returns ys[0] and we exponentially decay from one to the other
			// return Math.exp(logBase*(x-xs[0]))*(ys[0]-lower) + lower;
			if (useExponential)
				return FastMath.exp(logBase*(x-xs[0]))*(ys[0]-lower) + lower;
			else 
				return 1/(xs[0]-x+1)*(ys[0]-lower) + lower;
		} else if (x > xs[n-1]) {
			// In upper bound area infty returns upper xs[n-1] returns ys[n-1]
			// return Math.exp(logBase*(xs[n-1]-x))*(ys[n-1]-upper)+upper;
			if (useExponential)
				return FastMath.exp(logBase*(xs[n-1]-x))*(ys[n-1]-upper)+upper;
			else 
				return 1/(1 + x - xs[n-1])*(ys[n-1]-upper)+upper;
		} else if (Double.isNaN(x)) {
			return Double.NaN;
		} else {
			// We are in between
			for(int i=1; i < n; i++) {
				if (x <= xs[i]) {
					//pw linear between xs[i-1] and xs[i]
					return (ys[i]-ys[i-1]) / (xs[i] - xs[i-1]) * (x - xs[i-1]) + ys[i-1];
				}
			}
			throw new ArrayIndexOutOfBoundsException();
		}
	}

	public double evalInverse(double s) {
		int n = ys.length;
		if (s < ys[0]) {
			// In lower bound area -infty return lower xs[0] returns ys[0] and we exponentially decay from one to the other
			// return Math.exp(logBase*(x-xs[0]))*(ys[0]-lower) + lower;
			if (useExponential)
				return FastMath.exp(logBase*(s-ys[0]))*(xs[0]-lower) + lower;
			else
				return 1/(ys[0]-s+1)*(xs[0]-lower) + lower;
		} else if (s > ys[n-1]) {
			// In upper bound area infty returns upper xs[n-1] returns ys[n-1]
			// return Math.exp(logBase*(xs[n-1]-x))*(ys[n-1]-upper)+upper;
			if (useExponential)
				return FastMath.exp(logBase*(ys[n-1]-s))*(xs[n-1]-upper)+upper;
			else
				return 1/(1 + s - ys[n-1])*(xs[n-1]-upper)+ upper;
		} else if (Double.isNaN(s)) {
			return Double.NaN;
		} else {
			// We are in between: basically we inverse the x and y
			for(int i=1; i < n; i++) {
				if (s <= ys[i]) {
					//pw linear between xs[i-1] and xs[i]
					return (xs[i]-xs[i-1]) / (ys[i] - ys[i-1]) * (s - ys[i-1]) + xs[i-1];
				}
			}
			throw new ArrayIndexOutOfBoundsException();
		}
	}

}
