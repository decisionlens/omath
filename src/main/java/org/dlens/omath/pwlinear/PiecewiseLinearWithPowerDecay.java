package org.dlens.omath.pwlinear;

import org.dlens.omath.percentile.GradingCompatiblePercentile.Percentile;
import org.dlens.omath.percentile.GradingCompatiblePercentileException;

import static org.dlens.omath.percentile.GradingCompatiblePercentile.DEFAULT_ASYMPTOTIC_LOWER_BOUND;
import static org.dlens.omath.percentile.GradingCompatiblePercentile.DEFAULT_ASYMPTOTIC_UPPER_BOUND;
import static org.dlens.omath.pwlinear.PiecewiseLinearWithPowerDecay.DecayParameters.*;

//todo implement test and add more comments DL4-8928
public class PiecewiseLinearWithPowerDecay extends AbstractPwLinear {
    public final static int DEFAULT_POWER_FOR_DECAY_FUNCTION = 1;

    // score [0 - 1]

    private DecayParameters upperDecay;
    private DecayParameters lowerDecay;

    public PiecewiseLinearWithPowerDecay(Percentile percentile) {
        this(percentile.getX(), percentile.getY(), DEFAULT_ASYMPTOTIC_LOWER_BOUND, DEFAULT_ASYMPTOTIC_UPPER_BOUND, DEFAULT_POWER_FOR_DECAY_FUNCTION);
    }

    public PiecewiseLinearWithPowerDecay(double[] xs, double[] ys, double lower, double upper, double k) {
        if (xs.length != ys.length || xs.length < 1)
            throw new GradingCompatiblePercentileException("Invalid input parameters. xs should contain at least one element and shoud have the same lenght as yx");

        this.xs = xs;
        this.ys = ys;

        initializeDecayParams(k, lower, upper);
    }

    private void initializeDecayParams(double k, double lower, double upper) {
        if (xs.length == 1) {
            double m = ys[0] == 0 ? 1 : ys[0] / xs[0] * 2;
            this.lowerDecay = factory(xs[0], ys[0], m, k, lower);
            this.upperDecay = factory(xs[0], ys[0], m, k, upper);
        } else {
            int n = xs.length - 1; // the last index
            this.lowerDecay = factoryLower(xs[0], ys[0], xs[1], ys[1], k, lower);
            this.upperDecay = factoryUpper(xs[n - 1], ys[n - 1], xs[n], ys[n], k, upper);
        }
    }

    public double eval(double x) {
        int n = xs.length;

        if (Double.isNaN(x))
            return Double.NaN;
        if (x < xs[0])
            return lowerDecay.eval(x);
        if (x > xs[n - 1])
            return upperDecay.eval(x);
        if (xs.length == 1 && xs[0] == x)
            return ys[0];

        // We are in between
        for (int i = 1; i < n; i++)
            if (x <= xs[i])
                return (ys[i] - ys[i - 1]) / (xs[i] - xs[i - 1]) * (x - xs[i - 1]) + ys[i - 1];

        throw new GradingCompatiblePercentileException("");
    }

    public double evalInverse(double y) {
        int n = ys.length;

        if (Double.isNaN(y))
            return Double.NaN;
        if (y < ys[0])
            return lowerDecay.evalInverse(y);
        if (y > ys[n - 1])
            return upperDecay.evalInverse(y);
        if (ys.length == 1 && ys[0] == y)
            return xs[0];

        // We are in between
        for (int i = 1; i < n; i++)
            if (y <= ys[i])
                return (xs[i] - xs[i - 1]) / (ys[i] - ys[i - 1]) * (y - ys[i - 1]) + xs[i - 1];

        throw new ArrayIndexOutOfBoundsException();
    }

    static class DecayParameters {
        public double A;
        public double B;
        public double C;
        public double k;

        public static DecayParameters factory(double x, double y, double m, double k, double limit) {
            DecayParameters dp = new DecayParameters();
            double a = x;

            dp.C = limit;
            dp.B = a + k / m * (y - dp.C);
            dp.A = (y - dp.C) * Math.pow((a - dp.B), k);
            dp.k = k;
            return dp;
        }

        public static DecayParameters factoryLower(double x1, double y1, double x2, double y2, double k, double limit) {
            double m = (y2 - y1) / (x2 - x1);
            return factory(x1, y1, m, k, limit);
        }

        public static DecayParameters factoryUpper(double xn1, double yn1, double xn, double yn, double k, double limit) {
            double m = (yn - yn1) / (xn - xn1);
            return factory(xn, yn, m, k, limit);
        }

        public double eval(double x) {
            return A / Math.pow(x - B, k) + C;
        }

        public double evalInverse(double s) {
            return B + Math.pow(A / (s - C), 1d / k);
        }
    }
}