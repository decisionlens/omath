package org.dlens.omath.pwlinear;

public abstract class AbstractPwLinear {
    protected double[] xs;
    protected double[] ys;

    public abstract double eval(double x);
    public abstract double evalInverse(double s);

    public double[] getX(){
        return xs;
    }

    public double[] eval(double[] x) {
        double[] y = new double[x.length];
        for (int i = 0; i < x.length; i++)
            y[i] = eval(x[i]);
        return y;
    }

    public double[] evalInverse(double[] s) {
        double[] x = new double[s.length];
        for (int i = 0; i < s.length; i++)
            x[i] = evalInverse(s[i]);
        return x;
    }
}
