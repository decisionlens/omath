package org.dlens.omath.percentile;

public class GradingCompatiblePercentileException extends RuntimeException {
    public GradingCompatiblePercentileException(String msg) {
        super(msg);
    }
}
