package org.dlens.omath.percentile;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Iterator;
import java.util.TreeSet;

public class GradingCompatiblePercentile {
    public static final double DEFAULT_ASYMPTOTIC_LOWER_BOUND = 0;
    public static final double DEFAULT_ASYMPTOTIC_UPPER_BOUND = 1;
    public static final double DEFAULT_DECAY = 0.01;

    public static Percentile computeGradingCompatiblePercentiles(double[] x) {
        return computeGradingCompatiblePercentiles(x, DEFAULT_DECAY);
    }

    public static Percentile computeGradingCompatiblePercentiles(double[] x, double decay) {
        double[] xPrime = sortRemovingDuplicatesAndNan(x);
        return new Percentile(xPrime, computePercentiles(xPrime, decay));
    }

    private static double[] computePercentiles(double[] x, double decay) {
        if (x.length == 0)
            throw new GradingCompatiblePercentileException("You need at least one valid value in order to be able to compute the percentiles.");
        if (x.length == 1)
            return new double[]{0.5};

        double[] gcp = new double[x.length];
        // the asymptotic bounds are [0, 1]
        for (int i = 1; i < x.length - 1; i++) {
            gcp[i] = (double) i / (x.length - 1);
        }

        if (gcp[1] < decay)
            decay = gcp[1] / 2;

        gcp[0] = decay;
        gcp[x.length - 1] = 1 - decay;
        return gcp;
    }

    /**
     * Sort ASC and removes duplicates
     *
     * @param xs
     * @return
     */
    private static double[] sortRemovingDuplicatesAndNan(double[] xs) {
        TreeSet<Double> tree = new TreeSet<>();
        for (int i = 0; i < xs.length; i++)
            if (!Double.isNaN(xs[i]))
                tree.add(xs[i]);

        Iterator<Double> it = tree.iterator();
        double[] res = new double[tree.size()];
        int count = 0;

        while (it.hasNext())
            res[count++] = it.next();

        return res;
    }

    @Getter
    @AllArgsConstructor
    public static class Percentile {
        private double[] x;
        private double[] y;
    }

}
