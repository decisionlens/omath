package org.dlens.omath;

public enum NormalizationType {
    NONE,
    ZSCORE,
    IDEAL,
    SMALLEST_IS_ZERO;

    public double[] normalize(double[] vals) {
        switch (this) {
            case ZSCORE: {
                return OmathUtils.zscore(vals);
            }
            case IDEAL: {
                double max = OmathUtils.max(vals);
                double min = OmathUtils.min(vals);
                double diff = max - min;
                double factor = (diff == 0) ? 1.0 : 1.0/diff;
                double[] rval = new double[vals.length];
                for(int i=0; i < vals.length; i++)
                    rval[i] = (vals[i]-min) * factor;
                return rval;
            }
            case SMALLEST_IS_ZERO: {
                double min = OmathUtils.min(vals);
                double[] rval = new double[vals.length];
                for(int i=0; i < vals.length; i++)
                    rval[i] = vals[i]-min;
                return rval;
            }

            default: {
                return vals.clone();
            }
        }
    }
}
