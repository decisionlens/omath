package org.dlens.omath;

public class StandardLikertValue {
    public StandardLikertScale scale;
    public double originalValue;
    public double likertValue;


    public StandardLikertValue(double originalValue, double likertValue) {
        if ((likertValue < 0) || (likertValue > 1))
            throw new IllegalArgumentException();
        this.scale = StandardLikertScale.fromLikertValue(likertValue);
        this.originalValue = originalValue;
        this.likertValue = likertValue;
    }

    public StandardLikertValue(double originalValue, StandardLikertScale standardLikertScale) {
        this.originalValue = originalValue;
        this.scale = standardLikertScale;
        this.likertValue = standardLikertScale.doubleValue();
    }

    public static StandardLikertScale[] scales(StandardLikertValue[] scales) {
        if (scales == null)
            return null;
        StandardLikertScale[] rval = new StandardLikertScale[scales.length];
        for(int i=0; i<scales.length; i++)
            rval[i] = scales[i].scale;
        return rval;
    }

    public static double[] likertValues(StandardLikertValue[] scales) {
        if (scales == null)
            return null;
        double[] rval = new double[scales.length];
        for(int i=0; i<scales.length; i++)
            rval[i] = scales[i].likertValue;
        return rval;
    }
}
