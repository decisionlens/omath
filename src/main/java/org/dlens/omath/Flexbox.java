package org.dlens.omath;

import java.util.ArrayList;
import java.util.List;

public class Flexbox {
    public List<String> elements = new ArrayList<>();
    public static final String style = "<style> \n" +
            ".flex-container {\n" +
            "  display: flex;\n" +
            "   flex-wrap: wrap;\n"+
            "}\n" +
            "\n" +
            ".flex-container > div {\n" +
//            "  background-color: #f1f1f1;\n" +
            "  margin: 10px;\n" +
            "  padding: 0px;\n" +
            "  font-size: 30px;\n" +
            "}\n" +
            "</style>";
    public Flexbox(String ...elts) {
        if (elts == null)
            return;
        for(String elt : elts) {
            elements.add(elt);
        }
    }

    public void add(String ...elts) {
        if (elts == null)
            return;
        for(String elt : elts)
            elements.add(elt);
    }

    @Override
    public String toString() {
        StringBuffer rval = new StringBuffer(style);
        rval.append("<div class=\"flex-container\">\n");
        for(String elt: elements) {
            rval.append("<div>\n");
            rval.append(elt);
            rval.append("\n</div>\n");
        }
        rval.append("</div>");
        return rval.toString();
    }
}
