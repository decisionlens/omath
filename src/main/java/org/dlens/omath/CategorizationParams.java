package org.dlens.omath;

public class CategorizationParams {
    public boolean useClustering = true;
    public double clusteringEpsilon = .005;
    public double clusteringDelta = .05;
    public NormalizationType clusteringNormalize = NormalizationType.NONE;

    public static CategorizationParams factoryDefault() {
        return new CategorizationParams();
    }
}
