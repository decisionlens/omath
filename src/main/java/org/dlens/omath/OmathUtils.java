package org.dlens.omath;

import org.apache.commons.lang3.StringUtils;
import org.knowm.xchart.*;
import org.knowm.xchart.internal.chartpart.Chart;
import org.knowm.xchart.style.markers.SeriesMarkers;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;
import java.util.*;

import static java.lang.Math.abs;
import static org.knowm.xchart.BitmapEncoder.BitmapFormat;
import static org.knowm.xchart.BitmapEncoder.saveBitmap;


public class OmathUtils {
	private static final int MAX_CAT_WIDTH = 12;

	public static List<double[]> sorted(double[] xs, double[] ys) {
		List<double[]> rval = new ArrayList<>(xs.length);
		for(int i=0; i < xs.length; i++)
			rval.add(new double[] {xs[i], ys[i]});
		rval.sort(Comparator.comparingDouble(o -> o[0]));
		return rval;
	}
	
	public static List<Double> asList(double[] vs) {
		if (vs == null)
			return null;
		List<Double> rval = new ArrayList<>(vs.length);
		for(int i=0; i < vs.length; i++)
			rval.add(vs[i]);
		return rval;
	}
	
	public static List<Double> coords(List<double[]> vals, int coord) {
		ArrayList<Double> rval = new ArrayList<Double>(vals.size());
		for (double[] val : vals)
			rval.add(val[coord]);
		return rval;
		
	}

	public static double[] subset(double[] vals, List<Integer> indices) {
		double[] rval = new double[indices.size()];
		for(int i=0; i < indices.size(); i++) {
			rval[i] = vals[indices.get(i)];
		}
		return rval;
	}

	public static <T> T[] append(T[] l, T ...val) {
		if ((l == null) || (l.length == 0)) {
			if (val == null)
				return null;
			else
				return val.clone();
		} else if ((val==null) || (val.length==0)) {
			return l;
		}
		int size = l.length + val.length;
		Class<?> class1 = l.getClass().getComponentType();
		T[] rval = (T[]) Array.newInstance(class1, size);
		System.arraycopy(l, 0, rval, 0, l.length);
		System.arraycopy(val, 0, rval, l.length, val.length);
		return rval;
	}


	public static int[] append(int[] l, int ...val) {
		if ((l == null) || (l.length == 0)) {
			if (val == null)
				return null;
			else
				return val.clone();
		} else if ((val==null) || (val.length==0)) {
			return l;
		}
		int size = l.length + val.length;
		int[] rval = new int[size];
		System.arraycopy(l, 0, rval, 0, l.length);
		System.arraycopy(val, 0, rval, l.length, val.length);
		return rval;
	}

	public static double[] append(double[] l, double ...val) {
		if ((l == null) || (l.length == 0)) {
			if (val == null)
				return null;
			else
				return val.clone();
		} else if ((val==null) || (val.length==0)) {
			return l;
		}
		int size = l.length + val.length;
		double[] rval = new double[size];
		System.arraycopy(l, 0, rval, 0, l.length);
		System.arraycopy(val, 0, rval, l.length, val.length);
		return rval;
	}

	public static String letter(double val) {
		if (val > 0.8) {
			return "A";
		} else if (val > 0.6) {
			return "B";
		} else if (val > 0.4) {
			return "C";
		} else if (val > 0.2) {
			return "D";
		} else {
			return "F";
		}
	}
	
	public static String[] letter(double vals[]) {
		if (vals == null) {
			return null;
		} else {
			String[] rval = new String[vals.length];
			for(int i=0; i < vals.length; i++)
				rval[i] = letter(vals[i]);
			return rval;
		}
	}

	/**
	 * Returns sum(vals[i]) where levels[i]=level, i.e. sums the
	 * values of the vals vector where the levels vector equals the indicated level.
	 * @param vals
	 * @param levels
	 * @param level
	 * @return The given sum
	 */
	public static double sumover(double[] vals, int levels[], int level) {
		if (vals == null) {
			if (levels == null) {
				return 0;
			} else {
				throw new IllegalArgumentException("vals==null, but levels!=null");
			}
		} else if (levels==null) {
			throw new IllegalArgumentException("vals!=null but levels ==nul");
		} else if (vals.length != levels.length) {
			throw new IllegalArgumentException("(vals.length="+vals.length+
					"] is not equal to [levels.length="+levels.length+"]");
		}
		//Okay we are safe to do the calculation
		double rval=0;
		int size=vals.length;
		for(int i=0; i < size; i++)
			if (levels[i] == level)
				rval+=vals[i];
		return rval;
	}
	
	public static double percentOnAtLevel(double vals[], boolean states[], int levels[], int level) {
		if (vals == null)
			throw new IllegalArgumentException("values cannot be null");
		else if (states == null)
			throw new IllegalArgumentException("states cannot be null");
		else if (levels == null)
			throw new IllegalArgumentException("levels cannot be null");
		else if ((vals.length!=states.length) || (states.length!=levels.length))
			throw new IllegalArgumentException("values.length="+vals.length+" states.length="+states.length
					+" levels.length="+levels.length+" were not the same");
		//Okay, params are safe
		double rval=0;
		double total = 0;
		int size=vals.length;
		for(int i=0; i < size; i++) {
			if (states[i]) {
				if (levels[i] == level)
					rval+= Double.isNaN(vals[i]) ? 0 : vals[i];
				total += Double.isNaN(vals[i]) ? 0 : vals[i];
			}
		}
		if (total!=0)
			return rval/total;
		else
			return rval;
	}

	public static double[] percentOnAtLevel(double[] vals, boolean[] states, int[] levels) {
		double[] rval = new double[levels.length];
		int count = 0;
		for(int level: levels) {
			rval[count] = percentOnAtLevel(vals, states, levels, level);
			count++;
		}
		return rval;
	}

	public static boolean[] asBoolean(double[] ds) {
		if (ds == null)
			return null;
		boolean[] rval = new boolean[ds.length];
		for(int i=0; i < ds.length; i++)
			if (!Double.isNaN(ds[i]) && (ds[i] != 0))
				rval[i] = true;
		return rval;
	}

	public static Map<String, String> map(String ...args) {
		HashMap<String, String> rval = new HashMap<>();
		if (args==null)
			return rval;
		for(int i=0; i < args.length; i+=2) {
			rval.put(args[i], args[i+1]);
		}
		return rval;
	}
	
	public static boolean dominates(double[] vals1, double[] vals2, int[] indices) {
		if (vals1==null) 
			throw new IllegalArgumentException("vals1 cannot be null");
		else if  (vals2==null)
			throw new IllegalArgumentException("vals2 cannot be null");
		else if (vals1.length!=vals2.length)
			throw new IllegalArgumentException("vals1.length="+vals1.length+" was not equal to vals2.length="+vals2.length);
		boolean oneWasGreater=false;
		if (indices == null) {
			for(int i=0; i < vals1.length; i++) {
				if (vals1[i] < vals2[i])
					return false;
				else if (vals1[i] > vals2[i])
					oneWasGreater=true;
			}
			return oneWasGreater;
		} else {
			for(int i: indices) {
				if (vals1[i] < vals2[i])
					return false;
				else if (vals1[i] > vals2[i])
					oneWasGreater=true;
			}
			return oneWasGreater;
		}
	}


	public static boolean dominatesApprox(double[] vals1, double[] vals2, int[] indices, double absErr) {
		if (vals1==null) 
			throw new IllegalArgumentException("vals1 cannot be null");
		else if  (vals2==null)
			throw new IllegalArgumentException("vals2 cannot be null");
		else if (vals1.length!=vals2.length)
			throw new IllegalArgumentException("vals1.length="+vals1.length+" was not equal to vals2.length="+vals2.length);
		boolean oneWasGreater=false;
		if (indices == null) {
			for(int i=0; i < vals1.length; i++) {
				if (vals1[i] < vals2[i]+absErr)
					return false;
				else if (vals1[i] > vals2[i] + absErr)
					oneWasGreater=true;
			}
			return oneWasGreater;
		} else {
			for(int i: indices) {
				if (vals1[i] < vals2[i] + absErr )
					return false;
				else if (vals1[i] > vals2[i] + absErr)
					oneWasGreater=true;
			}
			return oneWasGreater;
		}
	}

	public static double mean(double[] vals) {
		double rval = 0;
		if ((vals==null) || (vals.length == 0))
			return 0;
		int size=vals.length;
		for(int i=0; i < size; i++)
			rval += vals[i];
		return rval/vals.length;
	}

	public static double stddev(double[] vals) {
		double mean = mean(vals);
		double rval = 0;
		for(double val : vals) {
			rval += (val - mean)*(val - mean);
		}
		if (vals.length > 0)
			rval /=vals.length;
		return Math.sqrt(rval);
	}

	public static double[] zscore(double[] vals) {
		double mean = mean(vals);
		double std = stddev(vals);
		return zscore(vals, mean, std);
	}

	public static double[] zscore(double[] vals, double mean, double std) {
		if (std == 0) {
			std = 1;
		}
		double[] rval = new double[vals.length];
		for(int i=0; i < rval.length; i++)
			rval[i] = (vals[i] - mean) / std;
		return rval;
	}

	public static double max(double[] vals) {
		double rval = Double.NEGATIVE_INFINITY;
		if ((vals==null) || (vals.length == 0))
			return rval;
		int size=vals.length;
		for(int i=0; i < size; i++)
			if (vals[i] > rval)
				rval = vals[i];
		return rval;
	}
	
	public static int maxIndex(double[] vals) {
		double rval = Double.NEGATIVE_INFINITY;
		int rvali = -1;
		if ((vals==null) || (vals.length == 0))
			return rvali;
		int size=vals.length;
		for(int i=0; i < size; i++) {
			if (vals[i] > rval)  {
				rval = vals[i];
				rvali = i;
			}
		}
		return rvali;
	}
	
	public static double min(double[] vals) {
		double rval = Double.POSITIVE_INFINITY;
		if ((vals==null) || (vals.length == 0))
			return rval;
		int size=vals.length;
		for(int i=0; i < size; i++)
			if (vals[i] < rval)
				rval = vals[i];
		return rval;
	}
	
	public static String statReport(double[] vals) {
		double mean = mean(vals);
		double max = max(vals);
		double min = min(vals);
		return "["+min+", "+max+"] mean="+mean;
	}
	
	public static List<Integer> range(int max) {
		max = Math.max(max, 0);
		List<Integer> rval = new ArrayList<>(max);
		for(int i=0; i < max; i++)
			rval.add(i);
		return rval;
	}

	public static int[] array(List<Integer> vals) {
		if (vals == null)
			return null;
		int[] rval = new int[vals.size()];
		int count=0;
		for(Integer val : vals) {
			rval[count] = val;
			count++;
		}
		return rval;
	}

	public static double[] doubleArray(List<Double> vals) {
		if (vals == null)
			return null;
		double[] rval = new double[vals.size()];
		int count=0;
		for(Double val : vals) {
			rval[count] = val;
			count++;
		}
		return rval;
	}

	public static double sumNaN(double[] costs) {
		double rval = 0;
		for(double cost : costs)
			if (!Double.isNaN(cost))
				rval+=cost;
		return rval;
	}






	public static String toHtml(Chart chart) {
		ByteArrayOutputStream bs = new ByteArrayOutputStream();
		try {
			saveBitmap(chart, bs, BitmapFormat.PNG);
			String a= Base64.getEncoder().encodeToString(bs.toByteArray());
			StringBuffer imgData = new StringBuffer(a);
			imgData.insert(0, "<img src=\"data:image/png;base64,");
			imgData.append("\"/>");
			return imgData.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static DialChart gradeChart(String title, Double percentValue) {
		return gradeChart(title, percentValue, null);
	}

	public static DialChart gradeChart(String title, Double percentValue, String displayValue) {
		DialChart chart = new DialChartBuilder().width(200).height(200).title(title).build();
		if (displayValue != null)
			chart.addSeries("Score", percentValue, displayValue);
		else
			chart.addSeries("Score", percentValue);
		chart.getStyler().setLegendVisible(false);
		chart.getStyler().setRedFrom(0.7);
		chart.getStyler().setGreenTo(0.3);
		chart.getStyler().setRedColor(new Color(0.0f, 1.0f, 0.0f));
		chart.getStyler().setGreenColor(new Color(1.0f, 0.0f, 0.0f));
		double[] ticks = {0.1, 0.3, 0.5, 0.7, 0.9};
		String[] ticknames = {"F", "D", "C", "B", "A"};
		chart.getStyler().setAxisTickValues(ticks);
		chart.getStyler().setAxisTickLabels(ticknames);
		return chart;
	}

	public static CategoryChart balanceBreakdownChart(String title, String xaxis, String yaxis, int width, int height, Map<String,Double> vals) {
		CategoryChart chart = new CategoryChartBuilder().width(width).height(height).
				title(title).
				xAxisTitle(xaxis).
				yAxisTitle(yaxis).build();
		// Customize Chart
		//chart.getStyler().setLegendPosition(LegendPosition.InsideNW);
		chart.getStyler().setHasAnnotations(true).setLegendVisible(false);
		String seriesName = " ";
		int ncats = vals.size();
		int stringMaxChars = (width - 50)/ncats  / 10;
		List<String> trimCatNames = abbrev(new ArrayList<>(vals.keySet()), stringMaxChars);
		chart.addSeries(seriesName, trimCatNames, new ArrayList<>(vals.values()));
		return chart;
	}

	public static List<String> abbrev(List<String> arrayList, int maxChars) {
		ArrayList<String> rval = new ArrayList<>(arrayList.size());
		for(String elt : arrayList) {
			rval.add(StringUtils.abbreviate(elt, maxChars));
		}
		return rval;
	}

	public static List<double[]> edgeOfPoints(double xs[], double ys[], int xsteps, int ysteps) {
		return edgeOfPoints(xs, ys, xsteps, ysteps, false);
	}

	public static List<double[]> edgeOfPoints(double xs[], double ys[], int xsteps, int ysteps, boolean includeMin) {
		double minx = min(xs);
		double maxx = max(xs);
		double deltax = (maxx - minx) / (xsteps);
		double miny = min(ys);
		double maxy = max(ys);
		double deltay = (maxy - miny) / (ysteps);
		double x1, x2, y1, y2;
		List<double[]> rval = new ArrayList<>(xsteps + ysteps);
		//First handle the x edge
		for(int i=0; i < xsteps; i++) {
			double maxYVal = Double.NEGATIVE_INFINITY;
			double minYVal = Double.POSITIVE_INFINITY;
			double theXVal = Double.NaN;
			x1 = minx + i * deltax;
			x2 = minx + (i+1)*deltax;
			for(int j=0; j < xs.length; j++) {
				if ((xs[j] <= x2)&&(xs[j]>= x1)) {
					//We are in the right area, see if we are the largest
					if (ys[j] > maxYVal) {
						maxYVal = ys[j];
						theXVal = xs[j];
					}
					if (ys[j] < minYVal) {
						minYVal = ys[j];
					}
				}
			}
			if (!Double.isNaN(theXVal)) {
				for(int j=0; j < ys.length; j++) {
					if ((xs[j] <= x2)&&(xs[j]>= x1)) {
						//We are in the right area, see if we are the largest
						if ((ys[j] == maxYVal) || (includeMin && (ys[j] == minYVal)))
							rval.add(new double[]{xs[j], ys[j]});
					}
				}
			}
		}
		//Now handle y edge
		for(int i=0; i < ysteps; i++) {
			double maxXVal = Double.NEGATIVE_INFINITY;
			double minXVal = Double.POSITIVE_INFINITY;
			double theYVal = Double.NaN;
			y1 = miny + i * deltay;
			y2 = miny + (i+1)*deltay;
			for(int j=0; j < ys.length; j++) {
				if ((ys[j] <= y2)&&(ys[j]>= y1)) {
					//We are in the right area, see if we are the largest
					if (xs[j] > maxXVal) {
						maxXVal = xs[j];
						theYVal = ys[j];
					}
					if (xs[j] < minXVal) {
						minXVal = xs[j];
					}
				}
			}
			if (!Double.isNaN(theYVal)) {
				for(int j=0; j < ys.length; j++) {
					if ((ys[j] <= y2) && (ys[j] >= y1)) {
						//We are in the right area, see if we are the largest
						if ((xs[j] == maxXVal) || (includeMin && (xs[j] == minXVal)))
							rval.add(new double[]{xs[j], ys[j]});
					}
				}
			}
		}
		return rval;
	}

	public static List<Integer> edgeOfPointsIndices(double xs[], double ys[], int xsteps, int ysteps, boolean includeMin) {
		double minx = min(xs);
		double maxx = max(xs);
		double deltax = (maxx - minx) / (xsteps);
		double miny = min(ys);
		double maxy = max(ys);
		double deltay = (maxy - miny) / (ysteps);
		double x1, x2, y1, y2;
		List<Integer> rval = new ArrayList<>(xsteps + ysteps);
		//First handle the x edge
		for(int i=0; i < xsteps; i++) {
			double maxYVal = Double.NEGATIVE_INFINITY;
			double minYVal = Double.POSITIVE_INFINITY;
			double theXVal = Double.NaN;
			x1 = minx + i * deltax;
			x2 = minx + (i+1)*deltax;
			for(int j=0; j < xs.length; j++) {
				if ((xs[j] <= x2)&&(xs[j]>= x1)) {
					//We are in the right area, see if we are the largest
					if (ys[j] > maxYVal) {
						maxYVal = ys[j];
						theXVal = xs[j];
					}
					if (ys[j] < minYVal) {
						minYVal = ys[j];
					}
				}
			}
			if (!Double.isNaN(theXVal)) {
				for(int j=0; j < ys.length; j++) {
					if ((xs[j] <= x2)&&(xs[j]>= x1)) {
						//We are in the right area, see if we are the largest
						if ((ys[j] == maxYVal) || (includeMin && (ys[j] == minYVal)))
							rval.add(j);
					}
				}
			}
		}
		//Now handle y edge
		for(int i=0; i < ysteps; i++) {
			double maxXVal = Double.NEGATIVE_INFINITY;
			double minXVal = Double.POSITIVE_INFINITY;
			double theYVal = Double.NaN;
			y1 = miny + i * deltay;
			y2 = miny + (i+1)*deltay;
			for(int j=0; j < ys.length; j++) {
				if ((ys[j] <= y2)&&(ys[j]>= y1)) {
					//We are in the right area, see if we are the largest
					if (xs[j] > maxXVal) {
						maxXVal = xs[j];
						theYVal = ys[j];
					}
					if (xs[j] < minXVal) {
						minXVal = xs[j];
					}
				}
			}
			if (!Double.isNaN(theYVal)) {
				for(int j=0; j < ys.length; j++) {
					if ((ys[j] <= y2) && (ys[j] >= y1)) {
						//We are in the right area, see if we are the largest
						if ((xs[j] == maxXVal) || (includeMin && (xs[j] == minXVal)))
							rval.add(j);
					}
				}
			}
		}
		return rval;
	}

	public static String convertToKMGNotation(double number, int ndigits) {
		String prepend = "";
		if (number == 0) {
			return "0";
		} else if (number < 0) {
			prepend = "-";
			number = -number;
		}
		String postfix="";
		if (number < 1e-21) {
			number /= 1e-24;
			postfix = " y";
		} else if (number < 1e-18) {
			number /= 1e-21;
			postfix = "z";
		} else if (number < 1e-15) {
			number /= 1e-18;
			postfix = "a";
		} else if (number < 1e-12) {
			number /= 1e-15;
			postfix = "f";
		} else if (number < 1e-9) {
			number /= 1e-12;
			postfix = "p";
		} else if (number < 1e-6) {
			number /= 1e-9;
			postfix = "p";
		} else if (number < 1e-3) {
			number /= 1e-6;
			postfix = "u";
		} else if (number < 1) {
			number /= 1e-3;
			postfix = "m";
		} else if (number < 1e3) {
			//Nothing to do
		} else if (number < 1e6) {
			number /= 1e3;
			postfix = "K";
		} else if (number < 1e9) {
			number /= 1e6;
			postfix = "M";
		} else if (number < 1e12) {
			number /= 1e9;
			postfix = "G";
		} else if (number < 1e15) {
			number /= 1e12;
			postfix = "T";
		} else if (number < 1e18) {
			number /= 1e15;
			postfix = "P";
		} else if (number < 1e21) {
			number /= 1e18;
			postfix = "E";
		} else if (number < 1e24) {
			number /= 1e21;
			postfix = "Z";
		} else  {
			number /= 1e24;
			postfix = "Y";
		}
		MathContext mathContext = new MathContext(ndigits);
		BigDecimal bd;
		bd = BigDecimal.valueOf(number);
		bd = bd.round(mathContext);
		return prepend+bd.toPlainString()+postfix;
	}


	public static XYChart boxWhiskers(double [][]multivals) {
		int count=1;
		XYChart chart = new XYChartBuilder().build();
		for(double[] vals : multivals) {
			double mmin = min(vals);
			double mmax = max(vals);
			double mean = mean(vals);
			double mid = mean;
			double stddev = stddev(vals);
			double top = mean + stddev;
			double bot = mean - stddev;
			double[] xs = {count, count};
			XYSeries s = chart.addSeries("a"+count,xs, new double[] {mmin, bot});
			s.setLineColor(new Color(0f, 0f, 0f, 0.2f));
			s.setMarkerColor(Color.BLACK);
			s.setMarker(SeriesMarkers.TRIANGLE_UP);
			s=chart.addSeries("b"+count,xs, new double[] {bot, mid});
			s.setLineColor(new Color(0f, 0f, 1f, 0.5f));
			s.setMarkerColor(Color.BLACK);
			s.setLineWidth(20);
			s=chart.addSeries("c"+count,xs, new double[] {mid, top});
			s.setLineColor(new Color(0f, 0f, 1f, 0.5f));
			s.setMarkerColor(Color.BLACK);
			s.setLineWidth(20);
			s=chart.addSeries("d"+count,xs, new double[] {top, mmax});
			s.setLineColor(new Color(0f, 0f, 0f, 0.2f));
			s.setMarkerColor(Color.BLACK);
			s.setMarker(SeriesMarkers.TRIANGLE_DOWN);
			count += 1;
		}
		chart.getStyler().setLegendVisible(false);
		chart.getStyler().setXAxisMin(0.5);
		chart.getStyler().setXAxisMax(count - 0.5);
		return chart;
	}


	public static Object[] normalizeKMGData(double[] data, Double maxval) {
		int size = data.length;
		double[] rval = data.clone();
		double mmax = (maxval == null) ? OmathUtils.max(data) : maxval;
		String decimalPattern="0";
		if ((mmax > 0) && (mmax < 10)) {
			decimalPattern = "0.00";
		} else if ((mmax >= 10)  && (mmax < 100)) {
			decimalPattern = "0.0";
		} else if ((mmax >= 100) && (mmax < 1000)) {
			decimalPattern = "0";
		} else if ((mmax >= 1000) && (mmax < 1e5)) {
			decimalPattern = "#.00K";
			for (int i=0; i < size; i++) {
				rval[i]/=1000d;
			}
		} else if ((mmax >= 1e5) && (mmax < 1e6)) {
			decimalPattern = "#K";
			for (int i=0; i < size; i++) {
				rval[i]/=1000d;
			}
		} else if ((mmax >= 1e6) && (mmax < 1e7)) {
			decimalPattern = "#.##M";
			for (int i=0; i < size; i++) {
				rval[i]/=1e6;
			}
		} else if ((mmax >= 1e7) && (mmax < 1e8)) {
			decimalPattern = "0.#M";
			for (int i=0; i < size; i++) {
				rval[i]/=1e6;
			}
		} else if ((mmax >= 1e8) && (mmax < 1e9)) {
			decimalPattern = "0M";
			for (int i=0; i < size; i++) {
				rval[i]/=1e6;
			}
		}
		return new Object[] {rval, decimalPattern};
	}

	/**
	 * Performs linear interpolation between the points (x1,y1) and (x2, y2).  If x1==x2 an error is
	 * thrown because we cannot linearly interpolate.
	 * @param x
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return
	 */
	public static double linear_interpolate(double x, double x1, double y1, double x2, double y2) {
		if (x1 == x2) {
			throw new IllegalArgumentException("x1 and x2 must differ");
		}
		//Our slope
		double m = (y2-y1)/(x2-x1);
		// We use the point slope formula for a line y-y1 = m(x-x1) or
		// y = y1 + m(x-x1) to get our return value
		return y1+m*(x-x1);
	}

	public static double[][] transposeTwoDimensionalArray(double[][] arr) {
		return transposeTwoDimensionalArray(arr, arr.length, arr[0].length);
	}

	public static double[][] transposeTwoDimensionalArray(double[][] arr, int rowsCount, int columnsCount) {
		int rowsLength = Math.min(arr.length, rowsCount);
		int columnsLength = Math.min(arr[0].length, columnsCount);

		double result[][] = new double[columnsLength][rowsLength];
		for (int rowIndex = 0; rowIndex < rowsLength; rowIndex++) {
			for (int columnIndex = 0; columnIndex < columnsLength; columnIndex++)
				result[columnIndex][rowIndex] = arr[rowIndex][columnIndex];
		}

		return result;
	}

	/**
	 * Computest the weighted median based on the sent array.
	 *
	 * @param values: sums[i] - value having the weight i + 1
	 * @return the weighted median
	 */
	public static double weightedMedian(double[] values) {
		double half_way = computeHalfWay(values);
		double[] middleArea = new double[values.length];
		int count = 0;
		double result = 0;
		double prev_area = 0;
		double area;
		double x;

		for (int i = 0; i < values.length; i++) {
			x = i + 1;
			// sums [i] - is the total costs for project with risk category [i + 1]
			area = prev_area + values[i];
			if (aproxEquals(area, half_way)) {
				middleArea[count++] = x;
			} else if (area > half_way) {
				if (count > 0) {
					middleArea[count++] = x;
					int aux = count;
					for (int j = 0; j < count; j++)
						result += middleArea[j];
					result /= aux;
				} else
					result = (prev_area == area) ? x : OmathUtils.linear_interpolate(half_way, prev_area, x - 0.5, area, x + 0.5);
				break;
			}
			prev_area = area;
		}
		return result;
	}

	/**
	 * The sum of the received array / 2
	 *
	 * @param sums
	 * @return
	 */
	private static double computeHalfWay(double[] sums) {
		double half_way = 0;
		for (double sum : sums)
			half_way += sum;
		return half_way / 2;
	}

	/**
	 * Checks weather a aprox. eq. to b. delta is the DEFAULT_DOUBLE_EQUALITY_DELTA
	 *
	 * @param a
	 * @param b
	 * @return true or false
	 */
	public static boolean aproxEquals(double a, double b) {
		return aproxEquals(a, b, 1e-15);
	}

	/**
	 * Checks weather a aprox. eq. to b. using the supplied delta
	 *
	 * @param a
	 * @param b
	 * @param delta
	 * @return
	 */
	public static boolean aproxEquals(double a, double b, double delta) {
		if (a == 0 && b == 0)
			return true;

		return (abs(a - b) / (abs(a) + abs(b)) / 2) < delta;
	}
}
