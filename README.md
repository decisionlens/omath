# Open source omath java library#

Welcome to the Open Source Math and general utilities library of Decision Lens.

### Features ###

* Standard math functions, including:
  * Basic stats on double arrays
  * Basic appending of arrays of objects or native types.
  * Many other features soon to be added.
* Version

### How do I get set up? ###

* Clone this repository
* Open this directory as a new maven project in eclipse

### Who do I talk to? ###

* Speak with Bill Adams at Decision Lens if you have any questions.
